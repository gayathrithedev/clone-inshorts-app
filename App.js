// @flow
import React, {useEffect} from 'react';
import {StatusBar, Platform} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Home from './src/screens/Home';
import Notification from './src/screens/Notification';
import Search from './src/screens/Search';
import ViewNews from './src/screens/ViewNews';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const ViewStack = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen name="My Feed" component={Home} headerMode="none" />
      <Stack.Screen name="ViewNews" component={ViewNews} />
    </Stack.Navigator>
  );
};

const Tabs = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#1a73e8',
      }}>
      <Tab.Screen
        name="Search"
        component={Search}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="compass" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Home"
        component={ViewStack}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons
              name="newspaper"
              color={color}
              size={size}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Notification"
        component={Notification}
        options={{
          tabBarLabel: 'Notification',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons
              name="bell-outline"
              color={color}
              size={size}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  return (
    <NavigationContainer>
      <>
        {Platform.OS === 'ios' && <StatusBar barStyle="light-content" />}
        <Tabs />
      </>
    </NavigationContainer>
  );
};

export default App;
